---
layout: handbook-page-toc
title: "Hacker News"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Hacker News is our most important social channel, so always treat it as a top priority. Any thread that is discussing our company's structure, values, product vision or any other sensitive blog post, articles, etc. is both [important and urgent](/handbook/marketing/community-relations/community-advocacy/guidelines/general#urgent-and-important-mentions).

_Every comment should get a response from someone from the company._ It is encouraged to use [this template](/handbook/marketing/community-relations/community-advocacy/workflows/involving-experts) and involve as many experts as necessary. If you or the expert don't know the answer to a comment/remark please share your thoughts because every remark should have at least one response.

In addition to the `HackerNews` view in our Community Zendesk, GitLab mentions on HackerNews are also tracked on the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) Slack channel.

## Workflow

1. Go through the tickets in Zendesk `HackerNews` view:
   - If the related comment doesn't require a response, use the `Mention` Zendesk macro
   - If you decide that one of our resident GitLab experts might provide additional context and clarification for the particular topic or that the related comment might provide useful feedback for them, please [involve an expert](/handbook/marketing/community-relations/community-advocacy/workflows/involving-experts/)
   - If you are able to provide a quick response/resource and decide that you don't have to involve an expert, reply on [news.ycombinator.com](https://news.ycombinator.com/) (not on Zendesk) using your personal Hacker News account. _Note that [the window to edit a comment is 2 hours](https://github.com/minimaxir/hacker-news-undocumented#editdelete-time-limits), afterwards you cannot edit or delete a Hackernews comment._
1. Ping `@sytse` in the `#community-relations` Slack channel if you judge his input is required, or in case of doubt


### Backup workflow 

If the Zendesk integration is broken, feel free to use the Slack alternative:

1. Find new mentions on the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) Slack channel and follow decision flow from the original workflow
1. Add a checkmark (`:heavy_check_mark:`) on every Slack message that you process to indicate that it was responded or decided that it doesn't require a response

![Hacker News channel workflow](/images/handbook/marketing/community-relations/hn-mentions.png){: .shadow}

## Best practices

When responding to a post about GitLab on Hacker News:

* Post a link to the thread in the `#community-relations` Slack channel and ping other `@advocates`.
* Don't post answers that are almost the same, link to the original one instead.
* Address multi-faceted comments by breaking them down and using points, numbering and quoting.
* When someone posts a HackerNews thread link, monitor that thread manually. Don't wait for the notifications in the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) channel, because sometimes they're delayed by a few hours.

<i class="fas fa-info-circle" aria-hidden="true" style="color: rgb(49, 112, 143)
;"></i> Always bear in mind the [social media guidelines for Hacker News](/handbook/marketing/social-media-guidelines/#hacker-news) in all your interactions with the site.
{: .alert .alert-info}

## Social media guidelines

- Never submit GitLab content to Hacker News. Submission gets more credibility if a non-GitLab Hacker News community member posts it, we should focus on making our posts interesting instead of on submitting it.
- Don't share links to Hacker News stories/comments on Slack or Twitter and ask others to upvote because it might set off the voting detector.
- Don't make the first comment on a HackerNews post about GitLab, wait for people to leave comments and ask questions.
- Avoid using corporate jargon like 'PeopleOps'.
- Always address the HackerNews community as peers. Be sure to always be modest and grateful in responses.
- If you comment yourself make sure it is interesting and relevant.
- Make yourself familiar with [A List of Hacker News's Undocumented Features and Behaviors](https://github.com/minimaxir/hacker-news-undocumented) to understand HackerNews behaviour and moderation rules
- Check the tone of your response: Don’t be defensive, but instead share your point of view. 
- Try to teach people something interesting they didn’t know already
- Add value to your post with data points or direct links

Note: You can find the full list of social media guidelines [here](/handbook/marketing/social-media-guidelines/)

## Automation

These mentions are piped to Community Zendesk and the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) Slack channel by [Zapier](https://zapier.com).
